package com.gildedrose;

class GildedRose {
    Item[] items;

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        for (Item item : items) {
            updateItemQuality(item);
        }
    }

    public void updateItemQuality(Item item) {
        ItemMutator itemMutator = new ItemMutator(item);

        itemMutator.applyQualityDegrade();
        itemMutator.applyQualityUpdateOfAgedBrie();
        itemMutator.applyQualityUpdateOfBackstagePasses();

        itemMutator.updateSellIn();

        itemMutator.copyTo(item);
    }
}
