package com.gildedrose;

public class ItemMutator {
    public static final String AGED_BRIE = "Aged Brie";
    public static final String BACKSTAGE_PASSES = "Backstage passes to a TAFKAL80ETC concert";
    public static final String SULFURAS = "Sulfuras, Hand of Ragnaros";
    public static final String CONJURED = "Conjured Mana Cake";

    private final Item item;

    public ItemMutator(Item input) {
        item = new Item(input.name, input.sellIn, input.quality);
    }

    public void applyQualityDegrade() {
        if (shouldDegrade()) {
            setQuality(item.quality + degradeValue());
        }
    }

    private boolean shouldDegrade() {
        return !isAgedBrie() && !isSulfuras() && !isBackstagePasses();
    }

    public void applyQualityUpdateOfAgedBrie() {
        if (isAgedBrie()) {
            int adjustment = isSellDatePassed() ? 2 : 1;
            setQuality(item.quality + adjustment);
        }
    }

    public void applyQualityUpdateOfBackstagePasses() {
        if (isBackstagePasses()) {
            if (isSellDatePassed()) {
                setQuality(0);
            } else {
                if (item.sellIn <= 10) {
                    if (item.sellIn <= 5) {
                        setQuality(item.quality + 3);
                    } else {
                        setQuality(item.quality + 2);
                    }
                } else {
                    setQuality(item.quality + 1);
                }
            }
        }
    }

    public void updateSellIn() {
        if (!isSulfuras()) {
            item.sellIn = item.sellIn - 1;
        }
    }

    public void copyTo(Item to) {
        to.quality = item.quality;
        to.sellIn = item.sellIn;
    }

    private int degradeValue() {
        int degradeValue = item.name.equals(CONJURED) ? -2 : -1;
        if (isSellDatePassed()) {
            return degradeValue * 2;
        }
        return degradeValue;
    }

    private boolean isAgedBrie() {
        return this.item.name.equals(AGED_BRIE);
    }

    private boolean isSulfuras() {
        return this.item.name.equals(SULFURAS);
    }

    private boolean isBackstagePasses() {
        return this.item.name.equals(BACKSTAGE_PASSES);
    }

    private boolean isSellDatePassed() {
        return item.sellIn <= 0;
    }

    private void setQuality(int value) {
        if (value < 0) {
            value = 0;
        }
        if (value > 50) {
            value = 50;
        }
        item.quality = value;
    }
}
