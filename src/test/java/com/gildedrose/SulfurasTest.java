package com.gildedrose;

import org.junit.jupiter.api.Test;

import static com.gildedrose.TestUtils.assertItemEquals;


public class SulfurasTest {
    @Test
    void qualityNeverChanges() {
        Item[] items = new Item[]{new Item("Sulfuras, Hand of Ragnaros", 10, 10)};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertItemEquals(app.items[0], new Item("Sulfuras, Hand of Ragnaros", 10, 10));
    }
}
