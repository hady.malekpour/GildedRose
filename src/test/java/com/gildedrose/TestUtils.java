package com.gildedrose;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestUtils {
    public static void assertItemEquals(Item expected, Item actual) {
        assertEquals(expected.name, actual.name);
        assertEquals(expected.quality, actual.quality);
        assertEquals(expected.sellIn, actual.sellIn);
    }
}
