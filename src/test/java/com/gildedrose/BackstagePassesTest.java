package com.gildedrose;

import org.junit.jupiter.api.Test;

import static com.gildedrose.TestUtils.assertItemEquals;


public class BackstagePassesTest {
    @Test
    void qualityIncrease() {
        Item[] items = new Item[]{new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20)};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertItemEquals(app.items[0], new Item("Backstage passes to a TAFKAL80ETC concert", 14, 21));
    }

    @Test
    void qualityIncreaseNotMoreThan50() {
        Item[] items = new Item[]{new Item("Backstage passes to a TAFKAL80ETC concert", 1, 50)};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertItemEquals(app.items[0], new Item("Backstage passes to a TAFKAL80ETC concert", 0, 50));
    }

    @Test
    void qualityIncreaseByTwo() {
        Item[] items = new Item[]{new Item("Backstage passes to a TAFKAL80ETC concert", 10, 5)};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertItemEquals(app.items[0], new Item("Backstage passes to a TAFKAL80ETC concert", 9, 7));
    }

    @Test
    void qualityIncreaseByThree() {
        Item[] items = new Item[]{new Item("Backstage passes to a TAFKAL80ETC concert", 5, 5)};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertItemEquals(app.items[0], new Item("Backstage passes to a TAFKAL80ETC concert", 4, 8));
    }

    @Test
    void qualitySetToZero() {
        Item[] items = new Item[]{new Item("Backstage passes to a TAFKAL80ETC concert", 0, 5)};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertItemEquals(app.items[0], new Item("Backstage passes to a TAFKAL80ETC concert", -1, 0));
    }
}
