package com.gildedrose;

import org.junit.jupiter.api.Test;

import static com.gildedrose.TestUtils.assertItemEquals;

public class ConjuredItemTest {

    @Test
    void qualityDecreaseByTwo() {
        Item[] items = new Item[]{new Item("Conjured Mana Cake", 10, 20)};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertItemEquals(app.items[0], new Item("Conjured Mana Cake", 9, 18));
    }

    @Test
    void qualityDecreaseNeverNegative() {
        Item[] items = new Item[]{new Item("Conjured Mana Cake", 1, 1)};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertItemEquals(app.items[0], new Item("Conjured Mana Cake", 0, 0));
    }

    @Test
    void qualityDecreaseTwiceFast() {
        Item[] items = new Item[]{new Item("Conjured Mana Cake", 0, 10)};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertItemEquals(app.items[0], new Item("Conjured Mana Cake", -1, 6));
    }
}
