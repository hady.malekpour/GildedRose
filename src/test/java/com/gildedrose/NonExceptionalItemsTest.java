package com.gildedrose;

import org.junit.jupiter.api.Test;

import static com.gildedrose.TestUtils.assertItemEquals;

public class NonExceptionalItemsTest {

    @Test
    void qualityDecrease() {
        Item[] items = new Item[]{new Item("+5 Dexterity Vest", 10, 20)};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertItemEquals(app.items[0], new Item("+5 Dexterity Vest", 9, 19));
    }

    @Test
    void qualityDecreaseNeverNegative() {
        Item[] items = new Item[]{new Item("+5 Dexterity Vest", 0, 0)};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertItemEquals(app.items[0], new Item("+5 Dexterity Vest", -1, 0));
    }

    @Test
    void qualityDecreaseTwiceFast() {
        Item[] items = new Item[]{new Item("Elixir of the Mongoose", 0, 10)};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertItemEquals(app.items[0], new Item("Elixir of the Mongoose", -1, 8));
    }
}
