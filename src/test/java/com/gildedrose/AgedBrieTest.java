package com.gildedrose;

import org.junit.jupiter.api.Test;

import static com.gildedrose.TestUtils.assertItemEquals;


public class AgedBrieTest {
    @Test
    void qualityIncrease() {
        Item[] items = new Item[]{new Item("Aged Brie", 10, 20)};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertItemEquals(app.items[0], new Item("Aged Brie", 9, 21));
    }

    @Test
    void qualityIncreaseNotMoreThan50() {
        Item[] items = new Item[]{new Item("Aged Brie", 1, 49)};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertItemEquals(app.items[0], new Item("Aged Brie", 0, 50));
    }

    @Test
    void qualityIncreaseTwiceFast() {
        Item[] items = new Item[]{new Item("Aged Brie", 0, 5)};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertItemEquals(app.items[0], new Item("Aged Brie", -1, 7));
    }

}
