package com.gildedrose;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class TexttestFixtureTest {

    @Test
    public void checkStateAfterThirtyDays() throws URISyntaxException, IOException {
        byte[] expected = Files.readAllBytes(Paths.get(getClass().getResource("/thirty_days.txt").toURI()));
        Assertions.assertEquals(new String(expected), buildStepsAfterDays(30));
    }

    public String buildStepsAfterDays(int days) {
        Item[] items = new Item[]{
            new Item("+5 Dexterity Vest", 10, 20),
            new Item("Aged Brie", 2, 0),
            new Item("Elixir of the Mongoose", 5, 7),
            new Item("Sulfuras, Hand of Ragnaros", 0, 80),
            new Item("Sulfuras, Hand of Ragnaros", -1, 80),
            new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20),
            new Item("Backstage passes to a TAFKAL80ETC concert", 10, 49),
            new Item("Backstage passes to a TAFKAL80ETC concert", 5, 49),
            new Item("Conjured Mana Cake", 3, 6)
        };

        StringBuilder builder = new StringBuilder();
        GildedRose app = new GildedRose(items);

        for (int i = 0; i <= days; i++) {
            builder.append("-------- day ").append(i).append(" --------").append("\n");
            builder.append("name, sellIn, quality").append("\n");
            for (Item item : items) {
                builder.append(item).append("\n");
            }
            builder.append("\n");
            app.updateQuality();
        }
        return builder.toString();
    }

}
